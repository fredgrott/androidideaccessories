AndroidIDEAccessories
---

Accessory files for Eclipse IDE for Android Developers as certain accessory files 
are incomplete in several locations. For example the spelling file supplied by 
the AOSP project for Eclipse is somewhat sparse.


# Implementation Notes

PMD files are for 5.1 and up.

# Project License

Apache License 2.0

# Created by

Fred Grott

[Blog](http://grottworkshop.blogspot.com)

[webiste](http://fredgrott.bitbucket.org)


